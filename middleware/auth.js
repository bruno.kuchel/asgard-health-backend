const jwt = require('jsonwebtoken')
const config = require('config')

module.exports = function (req, res, next) {
    const token = req.header('x-auth-token')

    if(!token){
        return res.status(401).json({ msg: 'Acceso no permitido: token inválido'})
    }

    try {
        jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
            if (error) {
                return res.status(401).json({ msg: 'Token inválido'})
            }
            req.user = decoded.user
            if (req.baseUrl == '/profile' && decoded.user.is_admin == false){
                return res.status(403).json({ msg: 'El usuario no es admin'})
            }
            next()
        })
        
    } catch (err) {
        console.error('Fallo en el middleware')
        res.status(500).json({ msg: 'Server error'})
        
    }
}